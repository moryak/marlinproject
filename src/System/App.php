<?php

namespace System;

use App\Controllers\MainController;
/**
 * Class App
 * @package System
 * @throws \ErrorException
 */
class App
{
    public static function run()
    {
        session_start();

        $urlPath = $_SERVER['REQUEST_URI'];
        $urlPathParts = explode('/',$urlPath);
        $controller = $urlPathParts[1];
        $actionAndParam = explode('?', $urlPathParts[2]);
        $action = $actionAndParam[0];
        //var_dump($action);
        $parameters = explode('=', $actionAndParam[1]);
        $parameters = [$parameters[0] => $parameters[1]];


        if(empty($controller) && empty($action) || $controller === 'main'){
            $controller = 'main';
            $action = $action ?: 'index';
        }

        $controller = 'App\\Controllers\\' . ucfirst($controller) . 'Controller';
       // var_dump($controller);
        $action = 'action' . ucfirst($action);
        //var_dump($action);
        if (!class_exists($controller)) {
            throw new \ErrorException('Controller does not exist');
        }

        $newController = new $controller;
        if(!method_exists($newController,$action)){
            throw new \ErrorException('action does not exist');
        }

        $newController->$action($parameters);


    }
}