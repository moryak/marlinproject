<?php


namespace System;


use PDO;

class Database
{
    private static $db;

    private function __construct()
    {

    }

    //singleton
    public static function getDB()
    {
        if (self::$db !== null) {
            return self::$db;
        }

        self::$db = new PDO('mysql:host=mysql;dbname=blog;charset=utf8', 'root', 'Qq123456789');

        return self::$db;
    }
}