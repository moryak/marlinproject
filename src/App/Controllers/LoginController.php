<?php

namespace App\Controllers;

use PDO;
use System\Database;
use System\View;

class LoginController extends BaseController
{

    public function actionLogin()
    {
        View::render('login');

    }

    public function actionUserLogin()
    {

        if ($this->checkEmail() && $this->checkPassword()) {
            $_SESSION['user'] = $this->getUserName();
        } else {
            unset($_SESSION['user']);
            return View::render('login', ['errors' => $this->getErrors()]);
        }

        redirect($_SERVER['/']);
    }

    private function checkEmail()
    {
        $email = trim($_POST['email']);

        $pdo = Database::getDB();
        $sql = "SELECT * FROM users WHERE email = :email LIMIT 1";
        $statement = $pdo->prepare($sql);
        $statement->bindParam('email', $email);
        $statement->execute();
        $result = $statement->fetchAll(PDO::FETCH_ASSOC);

        if ($result) {
            return true;
        }
        $this->setErrors('errorEmail', 'Введите корректный email');
        return false;
    }

    private function checkPassword()
    {
        $email = trim($_POST['email']);
        $password = trim($_POST['password']);
        $pdo = Database::getDB();
        $sql = "SELECT password FROM users WHERE email = :email LIMIT 1";
        $statement = $pdo->prepare($sql);
        $statement->bindParam('email', $email);
        $statement->execute();
        //ДЛЯ ТОГО ЧТОБЫ ПОЛУЧИТЬ 1 КОНЕКРЕТНУЮ ЗАПИСЬ В ДАННОМ СЛУЧАЕ ПАРОЛЬ
        // ИСПОЛЬЗУЕМ fetch PDO::FETCHLAZY возвращает объект
        $result = $statement->fetch(PDO::FETCH_LAZY);
        $passwordFromDB = $result->password;


        return password_verify($password, $passwordFromDB);
    }

    /**
     * @return string
     */
    private function getUserName()
    {
        $email = trim($_POST['email']);

        $pdo = Database::getDB();
        $sql = "SELECT * FROM users WHERE email = :email LIMIT 1";
        $statement = $pdo->prepare($sql);
        $statement->bindParam('email', $email);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_LAZY);

        return $userName = $result->name;
    }


}