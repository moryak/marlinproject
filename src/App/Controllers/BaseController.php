<?php


namespace App\Controllers;


class BaseController
{
    private $errors;

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param mixed $errors
     */
    public function setErrors($errorKey, $errorValue)
    {
        $this->errors[$errorKey] = $errorValue;
    }
}