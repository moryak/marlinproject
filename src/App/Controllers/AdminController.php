<?php


namespace App\Controllers;

use PDO;
use System\Database;
use System\View;

class AdminController
{
    /**
     * @throws \ErrorException
     */
    public function actionAdmin()
    {
        return View::render('admin', ['users' => $this->getUser()]);
    }

    private function getUser()
    {

        $pdo = Database::getDB();
        $sql = 'SELECT * FROM blog';
        $statement = $pdo->prepare($sql);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);

    }

    public function actionDelete()
    {
        $pdo = Database::getDB();
        $sql = 'DELETE FROM blog WHERE blog.id = :id';
        $statement = $pdo->prepare($sql);
       // $statement->bindParam('id', $_GET['id']);
        $statement->execute(['id' => $_GET['id']]);

        redirect('/admin/admin');

    }

    public function actionAllow()
    {
        $pdo = Database::getDB();
        //$sql = "UPDATE FROM blog SET status = 1 WHERE id =:id";
        $sql = 'UPDATE blog SET status = 1 WHERE id =:id';
        $statement = $pdo->prepare($sql);
        $statement->execute(['id' => $_GET['id']]);

        redirect('/admin/admin');
    }

    public function actionBlock()
    {
        $pdo = Database::getDB();
        $sql = 'UPDATE  blog SET status = 0 WHERE id =:id';
        $statement = $pdo->prepare($sql);
        $statement->execute(['id' => $_GET['id']]);

        redirect('/admin/admin');
    }
}