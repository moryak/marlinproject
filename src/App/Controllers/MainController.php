<?php


namespace App\Controllers;


use PDO;
use System\Database;
use System\View;

class MainController extends BaseController
{

    /**
     * @throws \ErrorException
     */
    public function actionIndex()
    {
        if ($_POST) {
            $name = $_POST['name'];
            $text = $_POST['text'];

            if (empty($name)) {
                $this->setErrors('errorName', 'Введите имя');
            }

            if (empty($text)) {
                $this->setErrors('errorText', 'Введите текст');
            }

            if (!empty($name && $text)) {
                $this->addComment($name, $text);

                redirect($_SERVER['REQUEST_URI']);
            }
        }

        return View::render('index', ['users' => $this->getUser(), 'errors' => $this->getErrors()]);
    }

    private function getUser()
    {
        $pdo = Database::getDB();
        $sql = 'SELECT * FROM blog ORDER BY blog.date DESC ';
        $statement = $pdo->prepare($sql);
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    private function addComment($name, $text)
    {
        $pdo = Database::getDB();
        $sql = 'INSERT INTO blog (name, text) values (:name,:text)';
        $statement = $pdo->prepare($sql);
        $statement->execute($_POST);
        $_SESSION['messageIsSends'] = true;
    }


}