<?php


namespace App\Controllers;


use System\Database;
use System\View;


class CommentController extends BaseController
{
    public function actionAdd()
    {
       $name = $_POST['name'];
       $text = $_POST['text'];
       $sessionName = $_SESSION['user'];

       if(isset($sessionName)){

           $this->insertComment($sessionName, $text);
       }

        if (!empty($name && $text)) {

            $this->insertComment($name, $text);
        }

        if (empty($name)) {
            $this->setErrors('errorName', 'Введите имя');
        }

        if (empty($text)) {
            $this->setErrors('errorText', 'Введите текст');
        }




        redirect('/');

        /**
         * Если есть авторизованный пользователь записываем имя с СЕССИИ
         * Если нет авторизованного пользователя
         */
    }

    private function insertComment($name, $text)
    {
        $pdo = Database::getDB();
        $sql = 'INSERT INTO blog (name, text) values (:name, :text)';
        $statement = $pdo->prepare($sql);
        $statement->bindParam('name',$name);
        $statement->bindParam('text',$text);
        $statement->execute();
        $_SESSION['messageIsSends'] = true;
    }


}