<?php

namespace App\Controllers;

use System\Database;
use System\View;


/**
 * Class SetController
 * @package App\Controllers
 */
class RegistrationController extends BaseController
{
    /**
     * @throws \ErrorException
     */
    public function actionRegister()
    {
        if ($_POST) {
            $email = $_POST['email'];

            if ($this->passwordValidate() && $this->emailValidate($email)) {
                $this->storeUser();

                redirect($_SERVER['REQUEST_URI']);
            }
        }

        return View::render('register', ['errors' => $this->getErrors()]);
    }

//    private function emailValidate($email)
//    {
//        $email = filter_var(trim($email), FILTER_VALIDATE_EMAIL);
//        if ($email != true) {
//            $this->setErrors('errorEmail', 'Введите корректный email');
//            return false;
//        }
//        return true;
//    }

    private function emailValidate($email)
    {
        $email = filter_var(trim($email), FILTER_VALIDATE_EMAIL);
        $pdo = Database::getDB();
        $sql = 'SELECT email FROM users WHERE email = :email';
        $statement = $pdo->prepare($sql);
        $statement->bindParam(':email', $email);
        $statement->execute();
        $result = $statement->fetchAll();


        if ($email != true) {
            $this->setErrors('errorEmail', 'Введите корректный email');
            return false;
        }

        if (!empty($result)) {
            $this->setErrors('errorEmail', 'Такой email уже существует');

            return false;
        }

        return true;
    }


    private function passwordValidate()
    {
        $password = trim($_POST['password']);
        $passwordCheck = trim($_POST['passwordCheck']);
        $regexp = preg_match("/^[a-z0-9_-]{6}$/",$password);

        if($regexp == 0){
            $this->setErrors('errorPassword', 'Пароли должны быть длиннее 6 символов');
            return false;
        }

        if ($password !== $passwordCheck) {
            $this->setErrors('errorPassword', 'Пароли должны совпадать');
            return false;
        }
        return true;
    }

    private function storeUser()
    {
        $userName = trim($_POST['name']);
        $password = password_hash(trim($_POST['password']), PASSWORD_DEFAULT);
        $email = trim($_POST['email']);

        $pdo = Database::getDB();
        $sql = 'INSERT INTO users (name, password, email) VALUES (:name, :password, :email)';
        //подготока SQL запроса
        $statement = $pdo->prepare($sql);
        //связывание пармаетров
        $statement->bindParam(':name', $userName);
        $statement->bindParam(':password', $password);
        $statement->bindParam(':email', $email);
        //выполнение запроса
        $statement->execute();

    }
}