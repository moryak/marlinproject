<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Comments</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->

    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="project/project">
                Project
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    <li class="nav-item">
                        <a class="nav-link" href="login/login">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="registration/register">Register</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <main class="py-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="card-header"><h3>Комментарии</h3></div>
                    <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <?php if (isset($_SESSION['messageIsSends'])):?>
                            <div class="alert alert-success" role="alert">
                                Комментарий успешно добавлен
                            </div>
                            <?php endif;?>
                            <?php foreach ($users as $user): ?>
                            <?php if ($user['status'] == 1): ?>
                            <div class="media">
                                <img src="/img/<?php echo $user['avatar'];?>" class="mr-3" alt="..." width="64" height="64">
                                <div class="media-body">
                                    <h5 class="mt-0"><?php echo $user['name'];?></h5>
                                    <span><small><?php echo date('d/m/Y', strtotime($user['date']));?></small></span>
                                    <p>
                                        <?php echo $user['text'];?>
                                    </p>
                                </div>
                            </div>
                                <?php endif;?>
                            <?php endforeach; ?>

                        </div>
                    </div>
                </div>

                <div class="col-md-12" style="margin-top: 20px;">
                    <div class="card">
                        <div class="card-header"><h3>Оставить комментарий</h3></div>

                        <div class="card-body">
                            <form method="post" action="Comment/add" >
                                <?php var_dump($_SESSION['user']); ?>
                                <?php if(!$_SESSION['user']): ?>
                                <div class="form-group">
                                    <?php if ($errors['errorName']):?>
                                        <div class="alert alert-danger" role="alert">
                                            Введите имя пользователя
                                        </div>
                                    <?php endif;?>
                                    <label for="exampleFormControlTextarea1">Имя</label>
                                    <input name="name" class="form-control" id="exampleFormControlTextarea1"/>
                                </div>
                                <?php endif; ?>

                                <div class="form-group">
                                    <?php if ($errors['errorText']):?>
                                        <div class="alert alert-danger" role="alert">
                                            Введите текст
                                        </div>
                                    <?php endif;?>
                                    <label for="exampleFormControlTextarea1">Сообщение</label>
                                    <textarea name="text" class="form-control" id="exampleFormControlTextarea1"
                                              rows="3"></textarea>
                                </div>
                                <button type="submit" class="btn btn-success">Отправить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
</body>
</html>
<?php
unset($_SESSION['messageIsSends']);
unset($_SESSION['errors']);
?>